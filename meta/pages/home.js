module.exports = {
	contentPlaceHolders: [
		{
			id: "banners-principais-desktop",
			objects: [
				{
					type: "banner",
					name: "Banner Principal Desktop 01",
					contents: [
						{
							name: "Banner Principal Desktop 01",
							active: true,
							type: "image",
							file: "banner-principal-desktop.png",
							width: 1540,
							height: 532,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
				{
					type: "banner",
					name: "Banner Principal Desktop 02",
					contents: [
						{
							name: "Banner Principal Desktop 02",
							active: true,
							type: "image",
							file: "banner-principal-desktop.png",
							width: 1540,
							height: 532,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
				{
					type: "banner",
					name: "Banner Principal Desktop 03",
					contents: [
						{
							name: "Banner Principal Desktop 03",
							active: true,
							type: "image",
							file: "banner-principal-desktop.png",
							width: 1540,
							height: 532,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
				{
					type: "banner",
					name: "Banner Principal Desktop 04",
					contents: [
						{
							name: "Banner Principal Desktop 04",
							active: true,
							type: "image",
							file: "banner-principal-desktop.png",
							width: 1540,
							height: 532,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
			],
		},

		{
			id: "banners-principais-mobile",
			objects: [
				{
					type: "banner",
					name: "Banner Principal Mobile 01",
					contents: [
						{
							name: "Banner Principal Mobile 01",
							active: true,
							type: "image",
							file: "banner-principal-mobile.png",
							width: 375,
							height: 342,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
				{
					type: "banner",
					name: "Banner Principal Mobile 02",
					contents: [
						{
							name: "Banner Principal Mobile 02",
							active: true,
							type: "image",
							file: "banner-principal-mobile.png",
							width: 375,
							height: 342,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
				{
					type: "banner",
					name: "Banner Principal Mobile 03",
					contents: [
						{
							name: "Banner Principal Mobile 03",
							active: true,
							type: "image",
							file: "banner-principal-mobile.png",
							width: 375,
							height: 342,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
				{
					type: "banner",
					name: "Banner Principal Mobile 04",
					contents: [
						{
							name: "Banner Principal Mobile 04",
							active: true,
							type: "image",
							file: "banner-principal-mobile.png",
							width: 375,
							height: 342,
							//   "category": "*",
							//   "brand": "*"
							//   "period": ""
						},
					],
				},
			],
		},

		{
			id: "prateleira-01",
			objects: [
				{
					type: "Produtos Relacionados",
					name: "CONFIRA NOSSAS NOVIDADES",
					properties: {
						layout: "romulosemiao-prateleira-padrao",
						type: "QuemViuViuTambem",
						"number-of-columns": 12,
						"number-of-items": 12,
					},
				},
			],
		},

	],
};
